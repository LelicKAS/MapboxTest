﻿using Android.App;
using Android.Widget;
using Android.OS;
using Mapbox.Maps;
using Mapbox.Camera;
using Mapbox.Geometry;
using Mapbox.Style.Sources;
using Mapbox.Services.Commons.Models;
using System.Collections.Generic;
using Mapbox.Services.Commons.GeoJson;
using Mapbox.Style.Layers;
using Android.Views;
using Android.Graphics;
using System;

namespace MapboxTest
{
    [Activity(Label = "MapboxTest", MainLauncher = true)]
    public class MainActivity : Activity, Android.Views.View.IOnTouchListener
    {
        private MapView _mapView = null;

        private MapboxMap _map = null;

        private GeoJsonSource _source = null;
        private FillLayer _layer = null;

        private List<Position> _positions = new List<Position>();

        private LatLng _lastLocation = null;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            Mapbox.MapboxAccountManager.Start(this, "pk.eyJ1IjoibGVsaWNrYXMiLCJhIjoiY2o0ZWoyeGFzMWJheTJxb3pndWc4dzQ3bSJ9.AZefoGcDEiEQI_btDFu8qg");

            var layout = FindViewById<LinearLayout>(Resource.Id.host);

            _mapView = new MapView(this);
            _mapView.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
            _mapView.SetOnTouchListener(this);
                
            layout.AddView(_mapView);
            _mapView.OnCreate(savedInstanceState);

            _map = await _mapView.GetMapAsync();

            var position = new CameraPosition.Builder()
                .Target(new LatLng(41.885, -87.679)) // Sets the new camera position
                .Zoom(11) // Sets the zoom
                .Build(); // Creates a CameraPosition from the builder

            _map.AnimateCamera(CameraUpdateFactory.NewCameraPosition(position), 1000);
        }

        protected override void OnResume()
        {
            base.OnResume();

            _mapView.OnResume();
        }

        private void StartSelection(LatLng location)
        {
            if (_source != null)
            {
                _map.RemoveLayer(_layer.Id);
                _map.RemoveSource(_source.Id);

                _positions.Clear();
            }

            _lastLocation = location;

            _positions.Add(Position.FromCoordinates(location.Latitude, location.Longitude));

            _source = new GeoJsonSource("source-selection", this.GetCollection());

            _map.AddSource(_source);


            _layer = new FillLayer("layer-selection", "source-selection");

            // The layer properties for our line. This is where we make the line dotted, set the 
            // color, etc. 
            _layer.SetProperties(
              //PropertyFactory.LineDasharray(new Java.Lang.Float[] { (Java.Lang.Float)0.01f, (Java.Lang.Float)2f }),
              //PropertyFactory.LineCap(Property.LineJoinRound),
              //PropertyFactory.LineJoin(Property.LineJoinRound),
              PropertyFactory.LineWidth((Java.Lang.Float)5f),
              PropertyFactory.LineColor(Android.Graphics.Color.Blue),
              PropertyFactory.FillColor(Android.Graphics.Color.Blue)
            );

            _map.AddLayer(_layer);

            Console.WriteLine($"{_positions.Count} ({location.Latitude}, {location.Longitude})");
        }

        private void MoveSelection(LatLng location)
        {
            if (!location.Equals(_lastLocation))
            {
                _lastLocation = location;

                _positions.Add(Position.FromCoordinates(location.Latitude, location.Longitude));

                _source.SetGeoJson(this.GetCollection());

                Console.WriteLine($"{_positions.Count} ({location.Latitude}, {location.Longitude})");
            }
        }

        private void EndSelection(LatLng location)
        {
            this.MoveSelection(location);
        }

        private FeatureCollection GetCollection()
        {
            var lineString = LineString.FromCoordinates(_positions);

            return FeatureCollection.FromFeatures(new Feature[] { Feature.FromGeometry(lineString) });
        }

        #region Android.Views.View.IOnTouchListener
        public bool OnTouch(View view, MotionEvent args)
        {
            var point = new PointF(args.GetX(), args.GetY());
            var location = _map.Projection.FromScreenLocation(point);

            if (args.Action == MotionEventActions.Down)
                this.StartSelection(location);
            else if (args.Action == MotionEventActions.Move)
                this.MoveSelection(location);
            else if (args.Action == MotionEventActions.Down)
                this.EndSelection(location);

            return true;
        }
        #endregion
    }
}

